package com.bluelotussoftware.guice;

import com.bluelotussoftware.guice.slf4j.SLF4JModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;

/**
 * An implementation of {@link GuiceServletContextListener} that injects
 * {@link SLF4JModule}.
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
public class GuiceServletContextListenerImpl extends GuiceServletContextListener {

    @Override
    protected Injector getInjector() {
        return Guice.createInjector(new SLF4JModule());
    }

}
