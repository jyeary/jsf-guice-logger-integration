package com.bluelotussoftware.guice;

import com.bluelotussoftware.guice.slf4j.SLF4JModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * This is based on the work by Dennis Byrne and Cagatay Civici from the blog
 * post <a
 * href="http://notdennisbyrne.blogspot.com/2007/09/integrating-guice-and-jsf.html">Integrating
 * Guice and JSF</a>.
 *
 * @author John Yeary
 * @version 1.0
 */
public class GuiceServletContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent event) {
        ServletContext ctx = event.getServletContext();
        Injector inject = Guice.createInjector(new SLF4JModule());
        ctx.setAttribute(Injector.class.getName(), inject);
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        ServletContext ctx = event.getServletContext();
        ctx.removeAttribute(Injector.class.getName());
    }
}
